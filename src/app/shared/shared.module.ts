import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageErrorComponent } from './pages/page-error/page-error.component';

@NgModule({
  declarations: [
    PageErrorComponent
  ],
  exports: [
    PageErrorComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }



