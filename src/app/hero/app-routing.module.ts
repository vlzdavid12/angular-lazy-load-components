import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AddComponent} from './pages/add/add.component';
import {HomeComponent} from './pages/home/home.component';
import {ListComponent} from './pages/list/list.component';
import {SearchComponent} from './pages/search/search.component';
import {ViewComponent} from './pages/view/view.component';
import {EditComponent} from './pages/edit/edit.component';

const routes: Routes = [{
  path: '',
  component: HomeComponent,
  children: [
    {
      path: 'list',
      component: ListComponent
    }, {
      path: 'add',
      component: AddComponent,
    },
    {
      path: 'edit/:id',
      component: EditComponent,
    }, {
      path: 'search',
      component: SearchComponent
    }, {
      path: 'view',
      component: ViewComponent
    }, {
      path: ':id',
      component: HomeComponent
    }, {
      path: '**',
      redirectTo: ''
    }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
