import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddComponent } from './pages/add/add.component';
import { SearchComponent } from './pages/search/search.component';
import { ViewComponent } from './pages/view/view.component';
import { HomeComponent } from './pages/home/home.component';
import { ListComponent } from './pages/list/list.component';
import {AppRoutingModule} from './app-routing.module';
import { EditComponent } from './pages/edit/edit.component';


@NgModule({
  declarations: [
    AddComponent,
    SearchComponent,
    ViewComponent,
    HomeComponent,
    ListComponent,
    EditComponent
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
  ]
})
export class HeroModule { }
